# -*- coding: utf-8 -*-
import tweepy

# API Authentication
auth = tweepy.OAuthHandler('x2qmlf03sd0JYTh6HlZWMR7V8', 'W4YpVoUVz5qv5Cp0y4L95fv0TghchPnjPPQyx5XzjQt7GUsyUu')
auth.set_access_token('1169752035761278977-YElPVAcu1lOvmU2smSF6a67Mryk5pe', 'KsnJ0TFwhdzlbeM5MrnunOmGXHwLszYVfKWQGh6nLSfY9')
api = tweepy.API(auth)

# Fetch tweets by username
def getTweets():
	# Collect tweets
	tweets = api.search(q="covid-19",lang="en",until="2021-05-18", count=100, tweet_mode='extended')
	tweet_list = []
	for tweet in tweets:
		print(tweet.full_text)
		tweet_list.append(tweet.full_text)
	return tweet_list

# Remove hashtags, mentions, links
def cleanTweets(tweets):
	clean_data = []
	for tweet in tweets:
	    item = ' '.join(word.lower() for word in tweet.split() \
	    	if not word.startswith('#') and \
	    	   not word.startswith('@') and \
	    	   not word.startswith('http') and \
	    	   not word.startswith('RT'))
	    if item == "" or item == "RT":
	        continue
	    clean_data.append(item)
	return clean_data

def export(filename, data, p):
    with open(filename, p) as output:
    	for line in data:
        	output.write(line)


if __name__ == "__main__":
    tweets = getTweets()
    cleans = cleanTweets(tweets=tweets)
    with open('./data/tweets.txt','a',encoding="utf-8") as output:
    	for line in cleans:
        	output.write(line+"\n")