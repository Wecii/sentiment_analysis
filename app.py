# -*- coding: utf-8 -*-

## Sentiment App
import time
import json
import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier

path = 'data/opinions.tsv'
data = pd.read_table(path,header=None,skiprows=1,names=['Sentiment','Review'])
X = data.Review
y = data.Sentiment

#Using CountVectorizer to convert text into tokens/features
vect = CountVectorizer(stop_words='english', ngram_range = (1,1), max_df = .80, min_df = 4)
X_train, X_test, y_train, y_test = train_test_split(X,y,random_state=1, test_size= 0.2)

#Using training data to transform text into counts of features for each message
vect.fit(X_train)
X_train_dtm = vect.transform(X_train) 
X_test_dtm = vect.transform(X_test)

#Accuracy using Naive Bayes Model
NB = MultinomialNB()
NB.fit(X_train_dtm, y_train)
y_pred = NB.predict(X_test_dtm)
print('\nNaive Bayes')
print('Accuracy Score: ',metrics.accuracy_score(y_test,y_pred)*100,'%',sep='')
print('Confusion Matrix: ',metrics.confusion_matrix(y_test,y_pred), sep = '\n')

#Accuracy using Logistic Regression Model
LR = LogisticRegression()
LR.fit(X_train_dtm, y_train)
y_pred = LR.predict(X_test_dtm)
print('\nLogistic Regression')
print('Accuracy Score: ',metrics.accuracy_score(y_test,y_pred)*100,'%',sep='')
print('Confusion Matrix: ',metrics.confusion_matrix(y_test,y_pred), sep = '\n')

#Accuracy using SVM Model
SVM = LinearSVC()
SVM.fit(X_train_dtm, y_train)
y_pred = SVM.predict(X_test_dtm)
print('\nSupport Vector Machine')
print('Accuracy Score: ',metrics.accuracy_score(y_test,y_pred)*100,'%',sep='')
print('Confusion Matrix: ',metrics.confusion_matrix(y_test,y_pred), sep = '\n')

#Accuracy using KNN Model
KNN = KNeighborsClassifier(n_neighbors = 3)
KNN.fit(X_train_dtm, y_train)
y_pred = KNN.predict(X_test_dtm)
print('\nK Nearest Neighbors (NN = 3)')
print('Accuracy Score: ',metrics.accuracy_score(y_test,y_pred)*100,'%',sep='')
print('Confusion Matrix: ',metrics.confusion_matrix(y_test,y_pred), sep = '\n')

#Naive Bayes Analysis
tokens_words = vect.get_feature_names()
print('\nAnalysis')
print('No. of tokens: ',len(tokens_words))
counts = NB.feature_count_
df_table = {'Token':tokens_words,'Negative': counts[0,:],'Positive': counts[1,:]}
tokens = pd.DataFrame(df_table, columns= ['Token','Positive','Negative'])
positives = len(tokens[tokens['Positive']>tokens['Negative']])
print('No. of positive tokens: ',positives)
print('No. of negative tokens: ',len(tokens_words)-positives)
#Check positivity/negativity of specific tokens
token_search = ['awesome']
print('\nSearch Results for token/s:',token_search)
print(tokens.loc[tokens['Token'].isin(token_search)])

#Analyse False Negatives (Actual: 1; Predicted: 0)(Predicted negative review for a positive review) 
print(X_test[ y_pred < y_test ])
#Analyse False Positives (Actual: 0; Predicted: 1)(Predicted positive review for a negative review) 
print(X_test[ y_pred > y_test ])

#Custom Test: Test a review on the best performing model (Logistic Regression)
trainingVector = CountVectorizer(stop_words='english', ngram_range = (1,1), max_df = .80, min_df = 5)
trainingVector.fit(X)
X_dtm = trainingVector.transform(X)

NB_complete = MultinomialNB()
NB_complete.fit(X_dtm, y)

LR_complete = LogisticRegression()
LR_complete.fit(X_dtm, y)

SVM_complete = LinearSVC()
SVM_complete.fit(X_dtm, y)

KNN_complete = KNeighborsClassifier(n_neighbors = 3)
KNN_complete.fit(X_dtm, y)


def classify_all(nb,lr,svm,knn):
      tweets = []
      with open('./data/tweets.txt','r', encoding="utf-8") as tweets_file:
            for line in tweets_file:
                  tweets.append(line)

      print("Total tweets count is "+str(len(tweets)))
      tags = ['NEGATIVE','POSITIVE']
      count = 0
      result = {
            "knn":{
                  "results":[],
                  "negatives":0,
                  "positives":0,
                  "ratio(N/P)":0
            },
            "svm":{
                  "results":[],
                  "negatives":0,
                  "positives":0,
                  "ratio(N/P)":0
            },
            "logr":{
                  "results":[],
                  "negatives":0,
                  "positives":0,
                  "ratio(N/P)":0
            },
            "nbay":{
                  "results":[],
                  "negatives":0,
                  "positives":0,
                  "ratio(%n/p)":0
            },
            "tweets":[]
      }
      for tweet in tweets:
            tweet_dtm = trainingVector.transform([tweet.strip()])

            predLabelLR = LR_complete.predict(tweet_dtm)
            predLabelNB = NB_complete.predict(tweet_dtm)
            predLabelSVM = SVM_complete.predict(tweet_dtm)
            predLabelKNN = KNN_complete.predict(tweet_dtm)

            result['knn']['results'].append(predLabelKNN[0])
            result['svm']['results'].append(predLabelSVM[0])
            result['logr']['results'].append(predLabelLR[0])
            result['nbay']['results'].append(predLabelNB[0])

            result['tweets'].append({"tweet":tweet.strip(),"result":{"knn":tags[predLabelKNN[0]], "svm":tags[predLabelSVM[0]], "logr":tags[predLabelLR[0]], "nbay":tags[predLabelNB[0]]}})
            count+=1
            print(""+str(len(tweets))+" / "+str(count)+" is done")

      for knn_res in result['knn']['results']:
            if knn_res == 1:
                  positives = result['knn']['positives']
                  positives += 1
                  result['knn']['positives'] = positives
            else:
                  negatives = result['knn']['negatives']
                  negatives += 1
                  result['knn']['negatives'] = negatives

      knn_pos_rat = 100* (result['knn']['positives'])/(result['knn']['positives']+result['knn']['negatives'])
      knn_neg_rat = 100* (result['knn']['negatives'])/(result['knn']['positives']+result['knn']['negatives'])
      with open('./data/result/knn.json', 'w+', encoding="utf-8") as knn_file:
            knn_file.write(json.dumps({"positives":result['knn']['positives'], "negatives":result['knn']['negatives'], "ratio(%n/p)":str(knn_neg_rat)+'/'+str(knn_pos_rat)}))



      for svm_res in result['svm']['results']:
            if svm_res == 1:
                  positives = result['svm']['positives']
                  positives += 1
                  result['svm']['positives'] = positives
            else:
                  negatives = result['svm']['negatives']
                  negatives += 1
                  result['svm']['negatives'] = negatives

      svm_pos_rat = 100* (result['svm']['positives'])/(result['svm']['positives']+result['svm']['negatives'])
      svm_neg_rat = 100* (result['svm']['negatives'])/(result['svm']['positives']+result['svm']['negatives'])
      with open('./data/result/svm.json', 'w+', encoding="utf-8") as svm_file:
            svm_file.write(json.dumps({"positives":result['svm']['positives'], "negatives":result['svm']['negatives'], "ratio(%n/p)":str(svm_neg_rat)+'/'+str(svm_pos_rat)}))


      for logr_res in result['logr']['results']:
            if logr_res == 1:
                  positives = result['logr']['positives']
                  positives += 1
                  result['logr']['positives'] = positives
            else:
                  negatives = result['logr']['negatives']
                  negatives += 1
                  result['logr']['negatives'] = negatives

      logr_pos_rat = 100* (result['logr']['positives'])/(result['logr']['positives']+result['logr']['negatives'])
      logr_neg_rat = 100* (result['logr']['negatives'])/(result['logr']['positives']+result['logr']['negatives'])
      with open('./data/result/logr.json', 'w+', encoding="utf-8") as logr_file:
            logr_file.write(json.dumps({"positives":result['logr']['positives'], "negatives":result['logr']['negatives'], "ratio(%n/p)":str(logr_neg_rat)+'/'+str(logr_pos_rat)}))


      for nbay_res in result['nbay']['results']:
            if nbay_res == 1:
                  positives = result['nbay']['positives']
                  positives += 1
                  result['nbay']['positives'] = positives
            else:
                  negatives = result['nbay']['negatives']
                  negatives += 1
                  result['nbay']['negatives'] = negatives

      nbay_pos_rat = 100* (result['nbay']['positives'])/(result['nbay']['positives']+result['nbay']['negatives'])
      nbay_neg_rat = 100* (result['nbay']['negatives'])/(result['nbay']['positives']+result['nbay']['negatives'])
      with open('./data/result/nbay.json', 'w+', encoding="utf-8") as nbay_file:
            nbay_file.write(json.dumps({"positives":result['nbay']['positives'], "negatives":result['nbay']['negatives'], "ratio(%n/p)":str(nbay_neg_rat)+'/'+str(nbay_pos_rat)}))


      with open('./data/result/all.json', 'w+', encoding="utf-8") as tw_file:
            for tw in result['tweets']:
                  json.dump(tw, tw_file, ensure_ascii=False)
                  tw_file.write('\n')

classify_all(NB_complete, LR_complete, SVM_complete, KNN_complete)

"""
#Input Review
print('\nTest a custom review message')
print('Enter review to be analysed: ', end=' ')
test = []
test.append(input())
test_dtm = trainingVector.transform(test)
predLabelLR = LR_complete.predict(test_dtm)
predLabelNB = NB_complete.predict(test_dtm)
predLabelSVM = SVM_complete.predict(test_dtm)
predLabelKNN = KNN_complete.predict(test_dtm)
tags = ['NEGATIVE','POSITIVE']


#Display Output
time.sleep(1.5)
print('The review is predicted with Logistic Regression Model',tags[predLabelLR[0]])
print('The review is predicted with Naive Bayes Model',tags[predLabelNB[0]])
print('The review is predicted with SVM Model',tags[predLabelSVM[0]])
print('The review is predicted with KNN Model',tags[predLabelKNN[0]])

Output:

Naive Bayes
Accuracy Score: 98.9161849711%
Confusion Matrix:
[[586  12]
 [  3 783]]

Logistic Regression
Accuracy Score: 99.3497109827%
Confusion Matrix:
[[593   5]
 [  4 782]]

Support Vector Machine
Accuracy Score: 99.0606936416%
Confusion Matrix:
[[592   6]
 [  7 779]]

K Nearest Neighbors (NN = 3)
Accuracy Score: 98.6994219653%
Confusion Matrix:
[[589   9]
 [  9 777]]

Analysis
No. of tokens:  294
No. of positive tokens:  143
No. of negative tokens:  151

Search Results for token/s: ['awesome']
      Token  Positive  Negative
11  awesome     896.0       1.0

Test a custom review message
Enter review to be analysed:  I really appreciate the details of the movie. It was an awesome experience.
The review is predicted Positive
"""
